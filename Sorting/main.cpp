#include "Manager.h"
#include "SelectionSort.h"
#include "BubbleSort.h"
#include "InsertionSort.h"

int main()
{
	Manager manager;
	manager.printNums();
	
	InsertionSort* sorter = new InsertionSort();
	manager.sort(sorter);

	manager.printNums();

	delete sorter;
	system("PAUSE");
	return 0;
}