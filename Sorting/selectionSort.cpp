#include "SelectionSort.h"

SelectionSort::SelectionSort()
{
}

SelectionSort::~SelectionSort()
{
}

void SelectionSort::sort(Container& container)
{
	for (int counter = 0; counter < container.size(); counter++)
	{
		Container::iterator smallestNum = container.begin() + counter;

		for (Container::iterator it = container.begin() + 1 + counter; it != container.end(); ++it)
		{
			if (*it < *smallestNum)
			{
				smallestNum = it;
			}
		}

		if (container.begin() + counter != smallestNum)
		{
			Helper::swap<Container::iterator>(container.begin() + counter, smallestNum);
		}
	}
}
