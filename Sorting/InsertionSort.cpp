#include "InsertionSort.h"

InsertionSort::InsertionSort()
{
}

InsertionSort::~InsertionSort()
{
}

void InsertionSort::sort(Container& container)
{
	int j = 0;

	for (int i = 1; i < container.size(); i++)
	{
		int curr = container[i];

		for (j = i - 1; j >= 0; j--)
		{
			if (container[j] > curr)
			{
				container[j + 1] = container[j];
			}
			else
			{
				break;
			}
		}
		container[j + 1] = curr;
	}
}
