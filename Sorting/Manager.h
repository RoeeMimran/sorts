#pragma once

#include <vector>
#include <time.h>
#include <iostream>
#include "Sorter.h"

#define VECTOR_SIZE 10

class Manager
{
private:
	Container _container;

public:
	// c'tor and d'tor
	Manager();
	~Manager();

	// helpers
	void resetContainer();
	void printNums();

	void sort(Sorter* sorter);
};

