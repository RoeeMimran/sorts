#include "Manager.h"

Manager::Manager()
{
	srand(time(NULL));
	this->resetContainer();
}

Manager::~Manager()
{
}


void Manager::sort(Sorter* sorter)
{
	sorter->sort(this->_container);
}

void Manager::resetContainer()
{
	this->_container = Container();
	for (int i = 0; i < VECTOR_SIZE; i++)
	{
		this->_container.push_back(rand() % (VECTOR_SIZE * 10));
	}
}

void Manager::printNums()
{
	for (int num : this->_container)
	{
		std::cout << num << " ";
	}
	std::cout << std::endl;
}


