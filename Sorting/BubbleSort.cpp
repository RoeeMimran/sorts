#include "BubbleSort.h"

BubbleSort::BubbleSort()
{
}

BubbleSort::~BubbleSort()
{
}

void BubbleSort::sort(Container& container)
{
	for (Container::iterator i = container.begin(); i != container.end(); ++i)
	{
		for (Container::iterator j = i + 1; j != container.end(); ++j)
		{
			if (*j < *i)
			{
				Helper::swap(i, j);
			}
		}
	}
}
