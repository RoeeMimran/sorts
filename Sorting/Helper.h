#pragma once

class Helper
{
public:
	template<class T>
	static void swap(T a, T b)
	{
		int temp = *a;
		*a = *b;
		*b = temp;
	}
};


