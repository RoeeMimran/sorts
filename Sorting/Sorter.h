#pragma once

#include <vector>
#include "Helper.h"

typedef std::vector<int> Container;

class Sorter
{
public:
	Sorter();
	~Sorter();

	void virtual sort(Container& container) = 0;
};

