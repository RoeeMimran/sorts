#pragma once

#include "Sorter.h"

class InsertionSort : public Sorter
{
public:
	InsertionSort();
	~InsertionSort();

	void virtual sort(Container& container);
};

