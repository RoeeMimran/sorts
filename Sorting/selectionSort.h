#pragma once

#include "Sorter.h"

class SelectionSort : public Sorter
{
public:
	SelectionSort();
	~SelectionSort();

	void virtual sort(Container& container);
};

