#pragma once

#include "Sorter.h"

class BubbleSort : public Sorter
{
public:
	BubbleSort();
	~BubbleSort();

	void virtual sort(Container& container);
};

